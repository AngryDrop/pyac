import win32api, win32con
import time

def click(x, y):
	win32api.SetCursorPos((x, y))
	win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)
	win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)

if __name__ == "__main__":
	abort = False
	
	while not(abort):
		if(win32api.GetKeyState(0x1B) == 1):
			abort = True
			break
		pos = win32api.GetCursorPos();
		print(pos)
		#click(pos)
